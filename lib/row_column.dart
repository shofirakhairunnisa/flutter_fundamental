import 'package:flutter/material.dart';

void main() => runApp(const RowColumnApp());

class RowColumnApp extends StatelessWidget {
  const RowColumnApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Latihan Row Column"),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const Text("Text 1"),
            const Text("Text 2"),
            const Text("Text 3"),
            Row(
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                const Text("Text 4"),
                const Text("Text 5"),
                RaisedButton(
                    child: const Text('Back'),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
              ],
            )
          ],
        ),
      ),
    );
  }
}
