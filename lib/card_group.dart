import 'package:flutter/material.dart';

void main() => runApp(const CardGroup());

class CardGroup extends StatelessWidget {
  const CardGroup({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.green,
          body: Container(
              margin: const EdgeInsets.all(10),
              child: ListView(
                children: [
                  buildCard(Icons.account_box, 'Account Box'),
                  buildCard(Icons.adb, 'Android'),
                  RaisedButton(
                    child: const Text('Back'),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                ],
              ))),
    );
  }

  Card buildCard(IconData iconData, String text) {
    return Card(
        elevation: 5,
        child: Row(
          children: [
            Container(margin: const EdgeInsets.all(5), child: Icon(iconData, color: Colors.green,)),
            Text(text)
          ],
        ));
  }
}
