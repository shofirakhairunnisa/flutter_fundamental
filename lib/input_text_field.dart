import 'package:flutter/material.dart';

void main() => runApp(const InputTextField());

class InputTextField extends StatefulWidget {
  const InputTextField({Key? key}) : super(key: key);

  @override
  _InputTextFieldState createState() => _InputTextFieldState();
}

class _InputTextFieldState extends State<InputTextField> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(title: const Text('Input Text Field')),
      body: Container(
          margin: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              TextField(
                decoration: InputDecoration(
                    icon: const Icon(Icons.account_tree),
                    prefixIcon: const Icon(Icons.account_tree),
                    labelText: 'Organisasi',
                    hintText: 'Nama Organisasi',
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50))),
                onChanged: (value) {
                  setState(() {});
                },
                controller: controller,
              ),
              Text(controller.text),
              RaisedButton(
                  child: const Text('Back'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ],
          )),
    ));
  }
}
