import 'package:flutter/material.dart';
import 'package:hello_world/multi_page/second_page.dart';
import 'package:hello_world/screen/learn_pages.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Main Page'),
      ),
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RaisedButton(
              child: const Text('Go to Second Page'),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const SecondPage();
                }));
              }),
          RaisedButton(
              child: const Text('Back'),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const LearnPages();
                }));
              }),
        ],
      )),
    );
  }
}
