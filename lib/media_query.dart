import 'package:flutter/material.dart';

void main() => runApp(const MediaQueryResponsive());

class MediaQueryResponsive extends StatelessWidget {
  const MediaQueryResponsive({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MainPage(),
    );
  }
}

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Latihan Media Query')),
        body: Column(
          children: [
            (MediaQuery.of(context).orientation == Orientation.portrait)
                ? Column(children: generateContainers())
                : Row(children: generateContainers()),
            RaisedButton(
                child: const Text('Back'),
                onPressed: () {
                  Navigator.pop(context);
                }),
          ],
        ));
  }

  List<Widget> generateContainers() {
    return [
      Container(
        color: Colors.red,
        width: 100,
        height: 100,
      ),
      Container(
        color: Colors.green,
        width: 100,
        height: 100,
      ),
      Container(
        color: Colors.blue,
        width: 100,
        height: 100,
      ),
    ];
  }
}

// untuk ambil size device
// Container(
//         color: Colors.green,
//         width: MediaQuery.of(context).size.width / 3,
//         height: MediaQuery.of(context).size.height / 2,
//       ),
