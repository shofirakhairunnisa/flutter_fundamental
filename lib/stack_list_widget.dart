import 'package:flutter/material.dart';

void main() => runApp(const StackAlignWidget());

class StackAlignWidget extends StatelessWidget {
  const StackAlignWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: const Text("Latihan Stack Align Widget"),
            ),
            body: Stack(
              children: [
                // background
                Column(
                  children: [
                    Flexible(
                        flex: 1,
                        child: Row(
                          children: [
                            Flexible(
                                flex: 1,
                                child: Container(
                                  color: Colors.white,
                                )),
                            Flexible(
                                flex: 1,
                                child: Container(
                                  color: Colors.black12,
                                ))
                          ],
                        )),
                    Flexible(
                        flex: 1,
                        child: Row(
                          children: [
                            Flexible(
                                flex: 1,
                                child: Container(
                                  color: Colors.black12,
                                )),
                            Flexible(
                                flex: 1,
                                child: Container(
                                  color: Colors.white,
                                ))
                          ],
                        )),
                  ],
                ),
                // Listview dengan text
                ListView(
                  children: [
                    Column(
                      children: [
                        Container(
                          child: const Text(
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ut mi quis justo fermentum mollis eget dignissim augue. Aenean lobortis vel elit non scelerisque. Proin ornare bibendum nisi, vel commodo nulla lobortis sit amet. Maecenas scelerisque suscipit eros in vehicula. In at est at arcu pulvinar condimentum. Vestibulum eros justo, pulvinar non aliquet pharetra, ornare a mauris. Duis in hendrerit sem.Praesent bibendum finibus augue, at ornare tellus malesuada eu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ut mi quis justo fermentum mollis eget dignissim augue. Aenean lobortis vel elit non scelerisque. Proin ornare bibendum nisi, vel commodo nulla lobortis sit amet. Maecenas scelerisque suscipit eros in vehicula. In at est at arcu pulvinar condimentum. Vestibulum eros justo, pulvinar non aliquet pharetra, ornare a mauris. Duis in hendrerit sem.Praesent bibendum finibus augue, at ornare tellus malesuada eu.',
                              style: TextStyle(fontSize: 22)),
                          margin: const EdgeInsets.all(20),
                        ),
                      ],
                    )
                  ],
                ),
                // Button
                Align(
                  alignment: const Alignment(-1, 0.5),
                  child: RaisedButton(
                      child: const Text('Back'),
                      color: Colors.amber,
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                )
              ],
            )));
  }
}
