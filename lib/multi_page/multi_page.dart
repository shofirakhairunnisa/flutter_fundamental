import 'package:flutter/material.dart';

import 'login_page.dart';
void main() => runApp(const MultiPage());

class MultiPage extends StatelessWidget {
  const MultiPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Test App',
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
    );
  }
}