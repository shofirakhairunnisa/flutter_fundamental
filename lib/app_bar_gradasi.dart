import 'package:flutter/material.dart';

void main() => runApp(const AppBarGradasi());

class AppBarGradasi extends StatelessWidget {
  const AppBarGradasi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        leading: const Icon(
          Icons.adb,
          color: Colors.white,
        ),
        title: const Text(
          'Appbar Example',
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {},
          ),
        ],
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Color(0xff0096ff), Color(0xff6610f2)],
              begin: FractionalOffset.topLeft,
              end: FractionalOffset.bottomRight,
            ),
            // image: DecorationImage(image: AssetImage('assets/pattern.png'), fit: BoxFit.none, repeat: ImageRepeat.repeat)
          ),
        ),
      ),
      body: RaisedButton(
          child: const Text('Back'),
          onPressed: () {
            Navigator.pop(context);
          }),
    ));
  }
}
