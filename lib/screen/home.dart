import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:hello_world/screen/login.dart';
import 'package:hello_world/api/api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:hello_world/widgets/nav-drawer.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>{
  late String fullname;
  late String mytoken;

  @override
  void initState(){
    _loadUserData();
    super.initState();
  }

  _loadUserData() async{
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    
    var storageUser = localStorage.getString('user');
    var storageToken = localStorage.getString('token');
    var user = jsonDecode(storageUser!);
    var token = jsonDecode(storageToken!);
    if(user != null) {
      setState(() {
        fullname = user['fullname'];
      });
    }
    if(token != null) {
      setState(() {
        mytoken = token['token'];
      });
    }else{
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context)=>const Login()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Test App'),
        backgroundColor: Colors.teal,
      ),
      drawer: const NavDrawer(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Hi, $fullname',
              style: const TextStyle(
                  fontWeight: FontWeight.bold
              ),
            ),
            const Center(
              // child: RaisedButton(
              //   elevation: 10,
              //   onPressed: (){
              //     logout();
              //   },
              //   color: Colors.teal,
              //   shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
              //   child: Text('Logout'),
              // ),
            ),
          ],
        ),
      ),
    );
  }

  // void logout() async{
  //   var res = await Network().getData('/auth/logout');
  //   var body = json.decode(res.body);
  //   if(body['success']){
  //     SharedPreferences localStorage = await SharedPreferences.getInstance();
  //     localStorage.remove('user');
  //     localStorage.remove('token');
  //     Navigator.push(
  //         context,
  //         MaterialPageRoute(builder: (context)=>Login()));
  //   }
  // }
}