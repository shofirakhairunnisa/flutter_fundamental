import 'package:flutter/material.dart';
import 'package:hello_world/auth/login_page.dart';
import 'package:hello_world/card_group2.dart';
import 'package:hello_world/inkwell_button_gradasi.dart';
import 'package:hello_world/multi_page/multi_page.dart';
import 'package:hello_world/stateless_statefull_widget.dart';
import '../app_bar_gradasi.dart';
import '../card_group.dart';
import '../container.dart';
import '../hello_world.dart';
import '../hero_cliprect_widget.dart';
import '../input_text_field.dart';
import '../media_query.dart';
import '../position_floating_act.dart';
import '../row_column.dart';
import '../stack_list_widget.dart';

class LearnPages extends StatelessWidget {
  const LearnPages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('History Learn Flutter'),backgroundColor: Colors.deepPurple[200],
      ),
      body: Center(
        child: Container(
          child: ListView(
            children: [
            buildButton(context, 'Hello World', (context) { return const HelloWorld(); }),
            buildButton(context, 'Container with Margin & Padding', (context) { return const ContainerApp(); }),
            buildButton(context, 'Row & Column', (context) { return const RowColumnApp(); }),
            buildButton(context, 'State Widget', (context) { return const StateWidget(); }),
            buildButton(context, 'Appbar Gradasi', (context) { return const AppBarGradasi(); }),
            buildButton(context, 'Card Widget 1', (context) { return const CardGroup(); }),
            buildButton(context, 'Stack & List Widget', (context) { return const StackAlignWidget(); }),
            buildButton(context, 'Media Query', (context) { return const MediaQueryResponsive(); }),
            buildButton(context, 'Input Text Field', (context) { return const InputTextField(); }),
            buildButton(context, 'Multi Pages', (context) { return const MultiPage(); }),
            buildButton(context, 'Position & Floating Action Button', (context) { return PositionFloatingAct(); }),
            buildButton(context, 'Inkwell Button Gradasi', (context) { return const InkwellGradasi(); }),
            buildButton(context, 'Card Widget 2', (context) { return const CardGroup2(); }),
            buildButton(context, 'Login', (context) { return AuthLogin(); }),
            buildButton(context, 'Hero Animation & ClipRRect', (context) { return HeroAnimation(); }),
          ]),
        ),
      ),
    );
  }

  RaisedButton buildButton(BuildContext context, String title, dynamic page) {
    return RaisedButton(
        child: Text(title),
        padding: const EdgeInsets.all(22),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: page));
        });
  }
}
