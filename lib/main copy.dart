import 'package:flutter/material.dart';
import 'package:hello_world/screen/login.dart';
import 'package:hello_world/screen/home.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:hello_world/api/api.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Test App',
      debugShowCheckedModeBanner: false,
      home: CheckAuth(),
    );
  }
}

class CheckAuth extends StatefulWidget {
  const CheckAuth({Key? key}) : super(key: key);
  @override
  _CheckAuthState createState() => _CheckAuthState();
}

class _CheckAuthState extends State<CheckAuth> {
  bool isAuth = false;
  @override
  void initState() {
    _checkIfHaveToken();
    // _checkIfLoggedIn();
    super.initState();
  }

  // void _checkIfLoggedIn() async{
  //   var res = await Network().getData('/auth/check');
  //   var body = json.decode(res.body);
  //   if(body['success']){
  //     setState(() {
  //       isAuth = true;
  //     });
  //   }
  // }

  void _checkIfHaveToken() async{
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    if(token != null){
      setState(() {
        isAuth = true;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    Widget child;
    if (isAuth) {
      child = const Home();
    } else {
      child = const Login();
    }
    return Scaffold(
      body: child,
    );
  }
}