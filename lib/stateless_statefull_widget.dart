import 'package:flutter/material.dart';

void main() => runApp(const StateWidget());

class StateWidget extends StatefulWidget {
  const StateWidget({Key? key}) : super(key: key);

  @override
  _StateWidgetState createState() => _StateWidgetState();
}

class _StateWidgetState extends State<StateWidget> {
  int number = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: const Text("Stateless dan Statefull"),
            ),
            body: Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(number.toString(),
                        style: TextStyle(fontSize: 20 + number.toDouble())),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        RaisedButton(
                            child: Text('+'),
                            onPressed: () {
                              setState(() {
                                number = number + 1;
                              });
                            }),
                        RaisedButton(
                            child: Text('-'),
                            onPressed: () {
                              setState(() {
                                number = number - 1;
                              });
                            }),
                      ],
                    ),
                    RaisedButton(
                        child: const Text('Back'),
                        onPressed: () {
                          Navigator.pop(context);
                        }),
                  ]),
            )));
  }
}
