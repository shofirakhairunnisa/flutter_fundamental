import 'package:flutter/material.dart';

void main() => runApp(const ContainerApp());

class ContainerApp extends StatelessWidget {
  const ContainerApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: const Text("Latihan Container"),
      ),
      body: Container(
          color: Colors.grey,
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(10),
          child: Container(
            width: 500,
            height: 800,
            child: RaisedButton(
                child: const Text('Back'),
                color: Colors.amberAccent,
                onPressed: () {
                  Navigator.pop(context);
                }),
          )),
    ));
  }
}
